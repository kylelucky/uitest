# _*_coding: utf-8_*_
#
# @Project_Name:UITest
# @File_name: test_mp_login
# @author: kyle
# @date: 2021/10/4 2:09
import logging

import pytest

import page
from page.page_in import PageIn
from tools.get_driver import GetDriver
from tools.get_log import GetLog
from tools.read_yaml import read_yaml

log = GetLog.get_loggor()


class TestMpLogin:

    def setup_class(self):
        driver = GetDriver.get_web_driver(page.url_mp)
        self.mp = PageIn(driver).page_get_PageMpLogin()

    def teardown_class(self):
        GetDriver.quit_web_driver()

    @pytest.mark.parametrize("username,code,expect", read_yaml("mp_login.yaml"))
    def test_mp_login(self, username, code, expect):
        self.mp.page_mp_login(username, code)
        # print("\n 获取的昵称为：", self.mp.page_get_nickname())
        try:
            assert expect == self.mp.page_get_nickname()
        except Exception as e:
            log.error("断言出错，错误信息：{}".format(e))
            print("错误原因", e)
            self.mp.base_get_img()
            raise
