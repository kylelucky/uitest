# _*_coding: utf-8_*_
#
# @Project_Name:UITest
# @File_name: page_in
# @author: kyle
# @date: 2021/10/4 1:13
from page.page_mp_login import PageMpLogin


class PageIn:

    def __init__(self, driver):
        self.driver = driver

    def page_get_PageMpLogin(self):
        return PageMpLogin(self.driver)
