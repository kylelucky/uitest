""" mp_config """
from selenium.webdriver.common.by import By

mp_password_btn = (By.XPATH, "/html/body/div[1]/div[2]/div/div[2]/div[2]/div[1]/ul/li[3]")
mp_username = (By.CSS_SELECTOR, "[placeholder='请输入邮箱/手机号码']")
mp_code = (By.CSS_SELECTOR, "[placeholder='请输入密码']")
mp_login_btn = (By.CSS_SELECTOR, ".el-button--primary")
mp_nickname = (By.XPATH, "/html/body/div[1]/div[2]/div[1]/div[3]/div[2]/section/a")

""" url """
url_mp = "https://www.nowcoder.com/login?callBack=https%3A%2F%2Fwww.nowcoder.com%2Fprofile%2F462521582"
