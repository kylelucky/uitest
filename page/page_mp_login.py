# _*_coding: utf-8_*_
#
# @Project_Name:UITest
# @File_name: page_mp_login
# @author: kyle
# @date: 2021/10/4 1:13
from time import sleep

import allure

from base.base import Base
import page
from tools.get_log import GetLog

log = GetLog.get_loggor()

class PageMpLogin(Base):

    # @allure.step("one step")
    def page_click_psw_btn(self):
        self.base_click(page.mp_password_btn)

    # @allure.step("two step")
    def page_input_username(self, username):
        self.base_input(page.mp_username, username)

    # @allure.step("three step")
    def page_input_code(self, code):
        self.base_input(page.mp_code, code)

    # @allure.step("four step")
    def page_click_login_btn(self):
        sleep(3)
        self.base_click(page.mp_login_btn)

    # @allure.step("five step")
    def page_get_nickname(self):
        return self.base_get_text(page.mp_nickname)

    # 组合业务
    def page_mp_login(self, username, code):
        log.info("正在调用vstudio登陆业务方法，用户名为：{} 密码：{}".format(username, code))
        self.page_click_psw_btn()
        self.page_input_username(username)
        self.page_input_code(code)
        self.page_click_login_btn()
